where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile



############################################################################
#
# Add any required modules here that come from startup scripts, etc.
#
############################################################################

REQUIRED += asyn busy calc

############################################################################
#
# If you want to exclude any architectures:
#
############################################################################

EXCLUDE_ARCHS += linux-ppc64e6500
EXCLUDE_ARCHS += linux-corei7-poky


############################################################################
#
# Relevant directories to point to files
#
############################################################################

APP:=mcaApp
APPDB:=$(APP)/Db
APPSRCMCA:=$(APP)/mcaSrc
APPSRCAMPTEK:=$(APP)/AmptekSrc
#APPSRCCANBERRA:=$(APP)/CanberraSrc
#APPSRCCANBERRASUP:=$(APP)/CanberraSupport
#APPSRCRONTEC:=$(APP)/RontecSrc
#APPSRCSIS:=$(APP)/SISSrc
#APPCMDS:=$(APP)/cmds


############################################################################
#
# Add any files that should be copied to $(module)/Db
#
############################################################################

TEMPLATES += $(wildcard $(APPDB)/*.db)
TEMPLATES += $(wildcard $(APPDB)/*.template)
TEMPLATES += $(wildcard $(APPDB)/*.req)
TEMPLATES += $(wildcard ../iocsh/*.req)

USR_INCLUDES += -I/usr/include/libusb-1.0
LIB_SYS_LIBS += usb-1.0

############################################################################
#
# Add any files that need to be compiled (e.g. .c, .cpp, .st, .stt)
#
############################################################################

AMPTEK_SRCS += AsciiCmdUtilities.cpp
AMPTEK_SRCS += ConsoleHelper.cpp
AMPTEK_SRCS += DP5Protocol.cpp
AMPTEK_SRCS += DP5Status.cpp
AMPTEK_SRCS += DppSocket.cpp
AMPTEK_SRCS += DppLibUsb.cpp
AMPTEK_SRCS += stringex.cpp
AMPTEK_SRCS += DppUtilities.cpp
AMPTEK_SRCS += ParsePacket.cpp
AMPTEK_SRCS += SendCommand.cpp
AMPTEK_SRCS += NetFinder.cpp

AMPTEK_SRCS += drvAmptek.cpp

SOURCES += $(addprefix $(APPSRCAMPTEK)/,$(AMPTEK_SRCS))

DBDS += $(APPSRCAMPTEK)/mcaAmptekSupport.dbd


SOURCES   += $(APPSRCMCA)/devMCA_soft.c
SOURCES   += $(APPSRCMCA)/devMcaAsyn.c
SOURCES   += $(APPSRCMCA)/drvFastSweep.cpp

DBDINC_SRCS += $(APPSRCMCA)/mcaRecord.c
DBDINC_DBDS = $(subst .c,.dbd,   $(DBDINC_SRCS:$(APPSRCMCA)/%=%))
DBDINC_HDRS = $(subst .c,.h,     $(DBDINC_SRCS:$(APPSRCMCA)/%=%))
DBDINC_DEPS = $(subst .c,$(DEP), $(DBDINC_SRCS:$(APPSRCMCA)/%=%))

HEADERS   += $(DBDINC_HDRS)
SOURCES   += $(DBDINC_SRCS)

#DBDS += mca.dbd,
DBDS += $(APPSRCMCA)/mcaSupport.dbd

mcaRecord$(OBJ): $(COMMON_DIR)/mcaRecord.h

$(DBDINC_DEPS): $(DBDINC_HDRS)


############################################################################
#
# Add any header files that should be included in the install (e.g.
# StreamDevice or asyn header files that are used by other modules)
#
############################################################################

HEADERS   += $(APPSRCMCA)/mca.h
HEADERS   += $(APPSRCMCA)/drvMca.h

############################################################################
#
# Add any startup scripts that should be installed in the base directory
#
############################################################################

SCRIPTS += $(wildcard ../iocsh/*.iocsh)


############################################################################
#
# If you have any .substitution files, and template files, add them here.
#
############################################################################

# SUBS=$(wildcard $(APPDB)/*.substitutions)
# TMPS=$(wildcard $(APPDB)/*.template)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)

.PHONY: vlibs
vlibs:
