# IOCSH files for MCA devices

## mca

`.iocsh` file for devices connected via Ethernet.

#### Required macros:

*   **DEVICENAME**
    *   ESS name, the same as in Naming service/CCDB
    *   string
*   **IPADDR**
    *   The IP address or fully qualified domain name of the device
    *   string

## mca_usb

`.iocsh` file for devices connected via USB.

#### Required macros:

*   **DEVICENAME**
    *   ESS name, the same as in Naming service/CCDB
    *   string
