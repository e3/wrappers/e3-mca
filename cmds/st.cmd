# This should be a test startup script
require mca
require essioc


iocshLoad("${essioc_DIR}/common_config.iocsh", "IOCNAME=test-mca-ioc")

iocshLoad("$(mca_DIR)/mca_eth.iocsh", "DEVICENAME=test-mca,IPADDR=localhost,AUTOSAVE_DIR=.")
