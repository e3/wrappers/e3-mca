# This should be a test startup script
require mca
require essioc


iocshLoad("${essioc_DIR}/common_config.iocsh", "IOCNAME=test-mca-ioc")

iocshLoad("$(mca_DIR)/mca_usb.iocsh", "DEVICENAME=test-mca,AUTOSAVE_DIR=.")
